import {Application, Container, Renderer} from "pixi.js";
import {Emitter} from "pixi-particles";
import {getCircleTexture} from "./App";

export class GameThree extends Container
{
    app: Application;
    emitter: Emitter;

    constructor(app: Application)
    {
        super();
        this.app = app;
        app.ticker.add(this.update, this);

        this.emitter = new Emitter(
            // The PIXI.Container to put the emitter in
            // if using blend modes, it's important to put this
            // on top of a bitmap, and not use the root stage Container
            this,

            // The collection of particle images to use
            [getCircleTexture(app.renderer as Renderer, 128)],

            // Emitter configuration, edit this to change the look
            // of the emitter
            {
                alpha: {
                    list: [
                        {
                            value: 0.0,
                            time: 0
                        },
                        {
                            value: 1,
                            time: 0.2
                        },
                        {
                            value: 0.0,
                            time: 1
                        }
                    ],
                    isStepped: false
                },
                "scale": {
                    list: [
                        {
                            value: 0.2,
                            time: 0
                        },
                        {
                            value: 0.4,
                            time: 0.2
                        },
                        {
                            value: 1,
                            time: 1
                        }
                    ],
                    "minimumScaleMultiplier": 1
                },
                color: {
                    list: [
                        {
                            value: "#ffbb00",
                            time: 0
                        },
                        {
                            value: "#ffbb00",
                            time: 0.1
                        },
                        {
                            value: "#ff3300",
                            time: 0.4
                        },
                        {
                            value: "#010101",
                            time: 1
                        }
                    ],
                    isStepped: false
                },
                "speed": {
                    list: [
                        {
                            value: 2,
                            time: 0
                        },
                        {
                            value: 1,
                            time: 1
                        }
                    ],
                    isStepped: false
                },
                "acceleration": {
                    "x": 0,
                    "y": -100
                },
                "maxSpeed": 0,
                "startRotation": {
                    "min": 0,
                    "max": 360
                },
                "noRotation": true,
                "rotationSpeed": {
                    "min": 0,
                    "max": 0
                },
                "lifetime": {
                    "min": 2.0,
                    "max": 3
                },
                "blendMode": "add",
                "frequency": 0.3,
                "emitterLifetime": -1,
                "maxParticles": 10,
                "pos": {
                    "x": 0,
                    "y": 0
                },
                "addAtBack": false,
                "spawnType": "point"
            }
        );

        this.emitter.emit = true;
    }

    update(dt)
    {
        this.emitter.update(this.app.ticker.elapsedMS*0.001);
    }

    destroy()
    {
        this.app.ticker.remove(this.update, this);
        this.emitter.destroy();
    }


}