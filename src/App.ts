import {Application, BaseRenderTexture, Container, Graphics, Renderer, RenderTexture, SCALE_MODES} from "pixi.js";
import {GameOne} from "./GameOne";
import {GameTwo} from "./GameTwo";
import {GameSelector} from "./GameSelector";
import {GameThree} from "./GameThree";

function ellipse(size: number, color: number)
{
    const ratio = 2.5;
    const graphic = new Graphics();
    graphic.beginFill(color);
    graphic.drawEllipse(0, 0, size / ratio, size);
    graphic.beginHole();

    const diff = size / 5;
    graphic.drawEllipse(0, 0, (size - diff) / ratio, size - diff / ratio);
    graphic.endHole();
    return graphic;
}

function circle(size: number, color: number)
{
    const graphic = new Graphics();
    graphic.beginFill(color);
    graphic.drawCircle(0, 0, size);
    graphic.endFill();
    return graphic;
}

function Logo(color: number)
{
    const logo = new Container();

    const el1 = ellipse(180, color);

    const el2 = ellipse(180, color);
    el2.rotation = Math.PI / 3;

    const el3 = ellipse(180, color);
    el3.rotation = -Math.PI / 3;


    logo.addChild(el1, el2, el3, circle(35, color));

    return logo;
}

function init(app: Application)
{
    const {stage, screen, ticker} = app;
    const game = new GameSelector(app);
    game.addGames(GameOne,"First task",-70);
    game.addGames(GameTwo,"Second task",0);
    game.addGames(GameThree,"Third task",70);

    game.x = app.screen.width * 0.5;
    game.y = app.screen.height * 0.5;
    stage.addChild(game);
}

export default function main(app: Application)
{
    init(app);
}

export function getRandomTexture(renderer: Renderer, w: number = 128, h: number = 128): RenderTexture
{
    let bg = new Graphics();
    bg.lineStyle(2, 0xFFFFFF, 1);
    bg.beginFill(0xFF6633, 1);
    bg.drawRect(0, 0, w, h);
    bg.endFill();
    const texture = new RenderTexture(new BaseRenderTexture({
        width: w,
        height: h,
        scaleMode: SCALE_MODES.LINEAR,
        resolution: 1
    }));
    renderer.render(bg, texture);
    return texture;
}


export function getCircleTexture(renderer: Renderer, r:number): RenderTexture
{
    let bg = new Graphics();
    bg.beginFill(0xFF6633, 1);
    bg.drawCircle(r, r, r);
    bg.endFill();
    const texture = new RenderTexture(new BaseRenderTexture({
        width: r*2,
        height: r*2,
        scaleMode: SCALE_MODES.LINEAR,
        resolution: 1
    }));
    renderer.render(bg, texture);
    return texture;
}
