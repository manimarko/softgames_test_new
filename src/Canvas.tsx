import React, { memo } from "react";
import { Application, IApplicationOptions } from "pixi.js";

type Props = IApplicationOptions & {
  init: (app: Application) => void;
};

export default memo(({ init, ...props }:Props ) => (
  <canvas
    ref={(view: HTMLCanvasElement) => init(new Application({ view, ...props }))}
  ></canvas>
));
