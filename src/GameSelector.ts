import {Application, Container, Text} from "pixi.js";

export class GameSelector extends Container
{
    app: Application;
    gameContainer: Container = new Container();
    ui: Container = new Container();

    constructor(app: Application)
    {
        super();
        this.app = app;
        this.addChild(this.gameContainer);
        this.addChild(this.ui);
    }

    addGames(c: any, name: string, posY: number): void
    {
        const gameButton = new Text(name, {fontFamily: "sans-serif", fontSize: "40px", fill: "white"});
        gameButton.y = posY;
        gameButton.x = -gameButton.width * 0.5;
        gameButton.interactive = true;
        gameButton.on("click", () => this.showGame(c));
        this.ui.addChild(gameButton);
    }

    showGame(c: any)
    {
        this.ui.visible = false;
        this.gameContainer.addChild(new c(this.app));
        const closeButton = new Text("close", {fontFamily: "sans-serif", fontSize: "40px", fill: "white", });
        closeButton.x = 800;
        closeButton.y = -400;
        closeButton.interactive = true;
        closeButton.on("click", () =>
        {
            this.hideGame();
        });
        this.gameContainer.addChild(closeButton);
    }

    hideGame()
    {
        this.ui.visible = true;
        while (this.gameContainer.children.length>0) {
            this.gameContainer.removeChildAt(0).destroy({
                children: true,
                texture: true,
                baseTexture: true
            })
        }
    }
}