import {Container, TextStyle} from "pixi.js";
import {HTMLText} from "./HTMLText";

export class GameTwo extends Container
{
    htmlText: HTMLText;
    intervalId;

    constructor()
    {
        super();
        const style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 40,
            wordWrapWidth: 800,
            wordWrap: true,
            fill: 0xFFFFFF,
            trim: false,
            align: "center",
            padding: 1
        });

        this.htmlText = new HTMLText("", style);

        this.addChild(this.htmlText);
        this.drawText(this.getRandomString());
        this.intervalId = setInterval(() =>
        {
            this.drawText(this.getRandomString())
        }, 2000)
    }

    getRandomString()
    {
        let str = "";
        const actions = [this.getRandomText, this.getRandomEmoji];
        for (let i = 0; i < 10; i++) {
            str += actions[Math.floor(Math.random() * actions.length)]();
        }
        return str;
    }

    getRandomEmoji()
    {
        return ' &#x1F68' + (Math.round(Math.random() * 9)).toString() + "; ";
    }

    getRandomText()
    {
        const texts = ["a", "b", "sfsdf", "asafs", "33sa", "dd", "ssa"];
        const text = texts[Math.floor(Math.random() * texts.length)];
        return `<p style="font: ${10 + Math.round(45 * Math.random())}px Georgia, sans-serif; color:#FF6633;">${text}</p>`
    }

    drawText(text: string): void
    {
        this.htmlText.text = text;
        this.htmlText.updateText(true);
        this.htmlText.x = -this.htmlText.width * 0.5;
        this.htmlText.y = -this.htmlText.height * 0.5;
    }

    public destroy(options?: { children?: boolean; texture?: boolean; baseTexture?: boolean })
    {
        clearInterval(this.intervalId);
        super.destroy(options);
    }
}