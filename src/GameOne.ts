import {Application, Container, Renderer, Sprite, Texture} from "pixi.js";
import gsap from "gsap";
import {getRandomTexture} from "./App";

export class GameOne extends Container
{
    static CARDS_COUNT = 144;
    cardIndex = 0;
    tween: GSAPTween | null = null;

    constructor(app: Application)
    {
        super();
        this.createCards(getRandomTexture(app.renderer as Renderer));
        this.moveCard();
    }

    createCards(texture: Texture)
    {
        for (let i = 0; i < GameOne.CARDS_COUNT; i++) {
            let sprite = Sprite.from(texture);
            this.addChild(sprite);
            sprite.pivot.x = sprite.width * 0.5;
            sprite.pivot.y = sprite.height * 0.5;
            const {x, y} = this.getCardPos(0, i);
            sprite.position.set(x, y);
        }
    }

    moveCard(): void
    {
        const child = this.children[this.cardIndex];
        if (!child) {
            return;
        }
        this.tween = gsap.to(child, {
            duration: 2, ...this.getCardPos(1, this.cardIndex), onComplete: () =>
            {
                this.moveCard();
            }
        });
        this.cardIndex++;
    }

    getCardPos(stackIndex: number, index: number): { x: number, y: number }
    {
        return {
            x: stackIndex === 0 ? -200 : 200,
            y: (stackIndex === 0 ? 1 : -1) * (index * 5 - GameOne.CARDS_COUNT * 5 * 0.5)
        }
    }

    destroy(options?: { children?: boolean; texture?: boolean; baseTexture?: boolean })
    {
        super.destroy(options);
        if (this.tween) {
            this.tween?.kill();
            this.tween = null;
        }
    }
}